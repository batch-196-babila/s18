function displaySum(numberOne, numberTwo) {
    let sum = numberOne + numberTwo;
    console.log("The displayed sum of " + numberOne + " and " + numberTwo);
    console.log(sum);
}
displaySum(5, 15);

function displayDifference(numberOne, numberTwo) {
    let difference = numberOne - numberTwo;
    console.log("The displayed sum of " + numberOne + " and " + numberTwo);
    console.log(difference);
}
displayDifference(15, 5);

function displayProduct(numberOne, numberTwo) {
    let product = numberOne * numberTwo;
    return product;
}

let resultProduct = displayProduct(50, 10);
console.log("The product of 50 and 10:");
console.log(resultProduct);

function displayQuotient(numberOne, numberTwo) {
    let quotient = numberOne / numberTwo;
    return quotient;
}

let resultQuotient = displayQuotient(50, 10);
console.log("The quotient of 50 and 10:");
console.log(resultQuotient);

function calculateCircleArea(radius) {
    let circleArea = (radius * radius) * 3.1416;
    return circleArea;
}

let resultCalculateCircleArea = calculateCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:")
console.log(resultCalculateCircleArea);

function calculateAverage(averageOne, averageTwo, averageThree, averageFour, ) {
    let averageVar = (averageOne + averageTwo + averageThree + averageFour) / 4;
    return averageVar;
}

let resultCalculateAverage = calculateAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80:");
console.log(resultCalculateAverage);

function checkIfPassed(score, totalScore) {
    // either way of the formula
    //let scorePercentage = (100 / totalScore) * score;
    let scorePercentage = (score / totalScore) * 100;
    let isPassed = scorePercentage >= 75;

    return isPassed;
}

let isPassingScore = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);