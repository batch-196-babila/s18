// Able to receive data w/o the use of global variable
// Name is a parameter / variable container exists only in function
function printName(name) {
    console.log("My name is " + name);
}
// Data passed into a function invocation can be received by the function
// Argument
printName("Juana");
printName("Jimin");

// Argumnent - data passed into the function through function invocation
// Stored within a container called a parameter

function printMyAge(age) {
    console.log("I am " + age)
}

printMyAge(25);
printMyAge(); // undefined

function checkDivisibilityBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

function printFavoriteSuperhero(superheroName) {
    console.log("My Favorite Superhero is " + superheroName);
}
printFavoriteSuperhero("Scarlet Witch");

function printMyFavoriteLanguage(language) {
    console.log("My favorite language is " + language);
}

printMyFavoriteLanguage("Javascript");
printMyFavoriteLanguage("Java");

// Multiple Arguments

function printFullname(firstName, middleName, lastName) {
    console.log(firstName + " " + middleName + " " + lastName);
}

printFullname("Juan", "Crisostomo", "Ibarra");

// Arguments contain accdg to the order it was passed

printFullname("Stephen", "Smith"); // undefine lastname
printFullname("Stephen", "Smith", "Curry");
printFullname("Stephen", "Smith", "Curry", "James"); // James will not be added

// Use variables as Arguments

let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullname(fName, mName, lName);

function favoriteSongs(song1, song2, song3, song4, song5) {
    console.log("1. " + song1);
    console.log("2. " + song2);
    console.log("3. " + song3);
    console.log("4. " + song4);
    console.log("5. " + song5);
}

favoriteSongs("In love", "pov", "Save your tears", "Stuck with U", "Die for you");

let fullName = printFullname("Mark","Joseph", "Lacdao");
console.log(fullName); // Mark...Lacdo with undefined

function returnFullName(firstName, middleName, lastName) {
    return firstName + " " + middleName + " " + lastName;
}
fullName = returnFullName("Ernest", "Go", "Will");
console.log(fullName);

// Functions with a return statement are able to return value and saved in a variables

console.log(fullName + " is my grandpa");

function returnPhilippinesAddress(city) {
    return city + ", Philippines";
}

let myFullAddress = returnPhilippinesAddress("Mandaluyong");
console.log(myFullAddress);

function checkDivisibilityBy4(num) {
    let remainder = num % 4;
    let isDivisibleBy4 = remainder === 0;

    // returns eitehr T or F
    return isDivisibleBy4;

    // return keyword ends the process of function
    console.log("I am run after the return"); // won't display
}

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);

/* Mini Act - debugging */

function createPlayerInfo(username, level, job) {
    return "Username: " + username + ", Level: " + level + ", Job: " + job;
}

let user1 = createPlayerInfo("white_night", 95, "Paladin");
console.log(user1);